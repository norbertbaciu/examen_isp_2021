import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class Ex2 extends JFrame {

    JButton button = new JButton();
    JTextArea textArea = new JTextArea();
    JTextField textField = new JTextField();

    Ex2() throws IOException {
        setTitle("Ex2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500, 500);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);

        textArea.setBounds(10,10,100,40);
        add(textArea);

        textField.setBounds(130,10,100,40);
        add(textField);

        button.setBounds(50,70,100,30);
        button.setText("Click");
        button.addActionListener(new SwapText());
        add(button);



    }

    class SwapText implements ActionListener {


        public void actionPerformed(ActionEvent e) {


            textArea.setText(textField.getText());


        }
    }

    public static void main(String[] args) throws IOException {
        new Ex2();
    }
}

