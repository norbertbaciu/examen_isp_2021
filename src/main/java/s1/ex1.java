package s1;

public class ex1 {
    public class Ex1 {

        class B{
            private String param;
            C c = new C(); //compozitie

            void thisIsAMethod(Z parameterZ){ //dependenta

            }
        }

        class Y{

            B b; //asociere

            void f(){

            }
        }

        class Z{
            void g(){

            }
        }

        class E{
            B b;

            E(B parameterB){
                this.b = parameterB;  //agregare
            }
        }

        class C{

        }
    }
}
